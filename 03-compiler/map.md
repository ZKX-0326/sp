# 編譯器

* [語法理論](./doc/grammar.md)
    * [BNF 語法範例](./doc/bnf.md)
* [編譯器基礎](./doc/compiler.md)
* [C 語言的語法、語意與執行環境](./doc/cgrammar.md)
* [運算式編譯器 -- exp](./01-exp0)
* [詞彙解析 -- lexer](./02-lexerd)
* [編譯器 -- compiler](./03-compiler)
    * [編譯器 -- 獨立的中間碼 (IR)](./04-compiler-ir)
    * [編譯器 -- 產生 hack 組合語言](./05-compiler-hack)
    * [編譯器 -- 包含中間碼執行功能](./06-compiler-run)

